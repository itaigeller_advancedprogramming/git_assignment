#include "stack.h"

void push(stack* s, int element)
{
	addToList(&(s->head), element);
}

int pop(stack *s)
{
	return removeFromList(&(s->head));
}

void initStack(stack *s)
{
	addToList(&(s->head), 0); //Since there's only a list pointer in the struct, its the only thing to initialize and there's no other option other than initializing it with a random value I chose
}

void cleanStack(stack *s)
{
	List* tmp = NULL;
	while (s->head) //For each node in the list
	{
		tmp = s->head;
		s->head = s->head->next;
		delete tmp;
	}
}