#ifndef STACK_H
#define STACK_H

#include "linkedList.h"

/* a positive-integer value stack, with no size limit */
typedef struct
{
	List* head;
} stack;

void push(stack *s, int element); //Adds an element to the top of the stack
int pop(stack *s); //Removes an element from the top of stack

void initStack(stack *s); //Initializes the stack's first node with the value 0
void cleanStack(stack *s); //Cleans all the nodes of the stack


#endif /*STACK_H*/