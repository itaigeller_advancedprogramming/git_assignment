#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>

using namespace std;

typedef unsigned int Uint;

/* a queue contains only positive integer values */
struct  queue
{
	Uint maxSize;
	Uint currSize;
	Uint* arr;
};

typedef struct queue queue;

void initQueue(queue *q, int maxSize); //Initializes the dynamic array
void cleanQueue(queue *q); //Deletes the allocated memory

void enqueue(queue *q, int newValue); //Inserts a new value into the queue
int dequeue(queue *q); //Returns the element in top of the queue, or -1 if empty

#endif /* QUEUE_H */