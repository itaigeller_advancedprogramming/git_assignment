#pragma once
#include <iostream>
typedef unsigned int Uint;

struct List
{
	Uint num;
	List* next;
};

typedef struct List List;

void addToList(List** head, Uint num); //Adds a number to the start of the list
int removeFromList(List** head); //Removes a number from the start of the list. If the list is empty returns -1

