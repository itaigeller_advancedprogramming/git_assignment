#include "utils.h"

void reverse(int *nums, int size)
{
	stack s = { 0 };
	Uint counter;
	initStack(&s);
	for (counter = 0; counter < size; counter++) //Inserting each array's cell content into the stack
	{
		push(&s, nums[counter]);
	}
	for (counter = 0; counter < size; counter++) //Popping each pushed value from the stack into an arrray's cell
	{
		nums[counter] = pop(&s);
	}
	cleanStack(&s);
}

int* reverse10()
{
	int* arr = new int[SIZE];
	for (int counter = SIZE - 1; counter >= 0; counter--) //Reading a value for each of the array's cells in the opposite order
	{
		cout << "Please insert a number:" << endl;
		cin >> arr[counter];
	}

	return arr;
}