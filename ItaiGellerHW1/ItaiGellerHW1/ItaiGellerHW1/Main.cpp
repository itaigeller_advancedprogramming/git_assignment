#pragma once
#include <iostream>
#include "queue.h"
#include "utils.h"

#define ADD 1
#define REMOVE 2
#define EXIT 3

void main()
{
	queue q = { 0 };
	int choice = 0;
	int newValue = 0;
	Uint counter = 0;
	stack s = { 0 };

	cout << "Question 1:" << endl;
	cout << "Please insert the max size of the queue:" << endl;
	cin >> newValue;
	initQueue(&q, newValue);
	while (choice != EXIT)
	{
		cout << "Please insert your choice:\n1. Insert a number into the queue\n2. Remove a number from the queue\n3. Exit" << endl;
		cin >> choice;
		switch (choice)
		{
			case ADD:
				cout << "Please insert the value which you want to add to the queue:" << endl;
				cin >> newValue;
				enqueue(&q, newValue);
				break;
			case REMOVE:
				if ((newValue = dequeue(&q)) != -1)
					cout << "The removed value is: " << newValue << endl;
				break;
			case EXIT:
				cout << "Goodbye! Have A Good Day!" << endl;
				cleanQueue(&q);
				break;
			default:
				cout << "Invalid input! Please insert a valid input." << endl;
		}
	}
	choice = 0;

	initStack(&s);
	cout << "Question 2 Part A + B:" << endl;
	while (choice != EXIT)
	{
		cout << "Please insert your choice:\n1. Insert a number into the stack\n2. Remove a number from the stack\n3. Exit" << endl;
		cin >> choice;
		switch (choice)
		{
		case ADD:
			cout << "Please insert the value which you want to add to the stack:" << endl;
			cin >> newValue;
			push(&s, newValue);
			break;
		case REMOVE:
			if ((newValue = pop(&s)) != -1)
				cout << "The removed value is: " << newValue << endl;
			else
				cout << "The stack is empty and no more numbers can be removed from it!" << endl;
			break;
		case EXIT:
			cout << "Goodbye! Have A Good Day!" << endl;
			cleanStack(&s);
			break;
		default:
			cout << "Invalid input! Please insert a valid input." << endl;
		}
	}

	cout << "Question 2 Part C:" << endl;
	cout << "Please insert the size of the array:" << endl;
	cin >> newValue;
	int* arr = new int[newValue];
	for (counter = 0; counter < newValue; counter++)
	{
		cout << "Please insert a number:" << endl;
		cin >> arr[counter];
	}
	reverse(arr, newValue);
	for (counter = 0; counter < newValue; counter++)
	{
		cout << arr[counter] << " ";
	}
	cout << endl;

	cout << "Question 2 Part D:" << endl;
	int* array = reverse10();
	for (counter = 0; counter < 10; counter++)
	{
		cout << array[counter] << " ";
	}
	cout << endl;
	delete array;

	system("pause");
}