#include "linkedList.h"

void addToList(List** head, Uint num)
{
	List* toAdd = new List; //The node to add at the beginning of the list
	toAdd->num = num;
	if (!(*head)) //If the list is empty
	{
		toAdd->next = NULL;
	}
	else
	{
		toAdd->next = *head;
	}
	*head = toAdd; //Updating the head's location
}

int removeFromList(List** head)
{
	if (!(*head)) return -1; //If the list is empty
	List* tmp = (*head)->next; //Temporarily saving the next node
	Uint retVal = (*head)->num;
	delete *head;
	*head = tmp; //Updating the head's location
	return retVal;
}
