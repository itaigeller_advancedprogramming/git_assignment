#ifndef REVERSE_H
#define REVERSE_H

#include "stack.h"

#define SIZE 10

using namespace std;

typedef unsigned int Uint;

void reverse(int *nums, int size); //Reverses the order of the numbers in a given array using the stack
int* reverse10();

#endif REVERSE_H