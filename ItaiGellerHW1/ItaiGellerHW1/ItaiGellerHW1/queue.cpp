#include "queue.h"

void initQueue(queue *q, int maxSize)
{
	q->maxSize = maxSize; //Allocating memory for the queue and the array it contains and setting the other variales to their initial values
	q->currSize = 0;
	q->arr = new Uint[maxSize];
}

void cleanQueue(queue *q)
{
	delete[] q->arr;
}

void enqueue(queue *q, int newValue)
{
	if (q->currSize == q->maxSize) //If the queue is full
	{
		cout << "The queue is full and no more numbers can be inserted into it!" << endl;
	}
	else
	{
		q->arr[q->currSize++] = newValue;
	}
}

int dequeue(queue *q)
{
	if (q->currSize == 0) //If the queue is empty
	{
		cout << "The queue is empty and no numbers can be removed from it!\n";
		return -1;
	}
	Uint tmp = q->arr[0];
	for (Uint counter = 0; counter < q->currSize - 1; counter++) //Transfering each number into the next cell in the queue
	{
		q->arr[counter] = q->arr[counter + 1];
	}
	q->currSize--;
	return tmp;
}

